import itertools
import math

import numpy as np


def intersection(p1, p2, p3, p4):
    vertices = [p1, p2, p3, p4]
    solution = (0, 0)
    if is_normal_shape(p1, p2, p3, p4):
        options = list(itertools.combinations(vertices, 2))
        for i in options:
            for j in options[len(vertices):]:
                try:
                    if j[0][0] - j[1][0] != 0:
                        m1 = float((i[0][1] - i[1][1]) / (i[0][0] - i[1][0]))
                        eq1 = (m1, -m1 * i[0][0] + i[0][1])
                        m2 = float((j[0][1] - j[1][1]) / (j[0][0] - j[1][0]))
                        eq2 = (m2, -m2 * j[1][0] + j[1][1])
                        a = np.array([[-eq1[0], 1], [-eq2[0], 1]])
                        b = np.array([eq1[1], eq2[1]])
                        solution = np.linalg.solve(a, b)
                        if min([i[0][0], i[1][0]]) < solution[0] < max([i[0][0], i[1][0]]) and min([i[0][1], i[1][1]]) < \
                                solution[1] < max([i[0][1], i[1][1]]):
                            solution = int(solution[0]), int(solution[1])
                            return solution
                        else:
                            solution = (0, 0)
                except:
                    {}
    return solution


def is_normal_shape(p1, p2, p3, p4):
    vertices = [p1, p2, p3, p4]
    edge1 = vector(vertices[len(vertices) - 1], vertices[len(vertices) - 2])
    edge2 = vector(vertices[0], vertices[len(vertices) - 1])
    normal = (0, 0, -1)
    positive = dot_product(cross_product(edge1, edge2), normal) > 0
    for i in range(1, len(vertices)):
        edge1 = edge2
        edge2 = vector(vertices[i], vertices[i - 1])
        if positive != (dot_product(cross_product(edge1, edge2), normal) > 0):
            return False
    return True


def vector(p1, p2):
    return p1[0] - p2[0], p1[1] - p2[1], 0


def dot_product(v1, v2):
    return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2]


def cross_product(v1, v2):
    return v1[1] * v2[2] - v1[2] * v2[1], v1[2] * v2[0] - v1[0] * v2[2], v1[0] * v2[1] - v1[1] * v2[0]


def degree_with_floor(p1, p2, p3, p4):
    vertices = [(p1[0], p1[1]), (p2[0], p2[1]), (p3[0], p3[1]), (p4[0], p4[1])]
    d1 = list(filter(lambda x: x[1] == max([i[1] for i in vertices]), vertices))[0]
    vertices.remove(d1)
    d2 = list(filter(lambda x: x[1] == max([i[1] for i in vertices]), vertices))[0]
    angle = np.rad2deg(np.arctan2(d2[1] - d1[1], d2[0] - d1[0]))
    # assuming the camera pixels starts from top!
    return abs(angle)


def rectangle_get_top_left_down_right(vertices):
    def sort_x(x):
        return x[0]

    def sort_y(x):
        return x[1]

    sorted_vertices = sorted(vertices, key=sort_x)[:2]
    top_left = list(filter(lambda x: x[1] == min([i[1] for i in sorted_vertices]), sorted_vertices))[0]

    sorted_vertices = sorted(vertices, key=sort_y, reverse=True)[:2]
    bottom_right = list(filter(lambda x: x[0] == max([i[0] for i in sorted_vertices]), sorted_vertices))[0]
    print(top_left, bottom_right)
    return top_left, bottom_right


def get_angle_of_vector(x, y):
    if x == y == 0:
        return 0
    if x == 0:
        return math.radians(90) if y > 0 else math.radians(270)
    if y == 0:
        return math.radians(0) if x > 0 else math.radians(180)

    abs_angle = math.atan(abs(y)/abs(x))
    if x > 0 and y > 0:
        return abs_angle
    if x > 0 and y < 0:
        return math.radians(360) - abs_angle
    if x < 0 and y > 0:
        return math.radians(90) - abs_angle
    if x < 0 and y < 0:
        return math.radians(180) + abs_angle
