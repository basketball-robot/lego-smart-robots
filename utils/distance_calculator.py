

def distance_to_camera(knownWidth, focalLength, pixelWidth):
    # compute and return the distance from the maker to the camera
    return (knownWidth * focalLength) / pixelWidth


def dimension_of_rectangle(rect):
    x_max = max([rect[i][0][0] for i in range(4)])
    x_min = min([rect[i][0][0] for i in range(4)])
    y_max = max([rect[i][0][1] for i in range(4)])
    y_min = min([rect[i][0][1] for i in range(4)])
    width = int(abs(x_max - x_min))
    height = int(abs(y_max - y_min))
    return width, height


def area_of_rectangle(rect):
    width, height = dimension_of_rectangle(rect)
    return width * height