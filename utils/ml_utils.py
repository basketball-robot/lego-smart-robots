import cv2
import numpy as np

import config


def make_prediction(image, model_name):
    from tensorflow.keras.preprocessing.image import img_to_array
    from tensorflow.keras.models import load_model

    model = load_model(config.PROJECT_PATH + '/Basket-Ball-Robot/ml_models/' + model_name)

    frame = cv2.resize(image, (32, 32))
    frame = frame.astype("float") / 255.0
    frame = img_to_array(frame)
    frame = np.expand_dims(frame, axis=0)
    # make predictions on the input image
    pred = model.predict(frame)
    pred = pred.argmax(axis=1)[0]

    return pred


def is_floor(frame):
    pred = make_prediction(frame, config.ML_MODEL_NAME)

    if pred == 2:
        return True
    return False


def predict_brick(image):
    pred = make_prediction(image, config.BRICK_MODEL_NAME)

    return pred
