import math

import cv2
import imutils

from utils.geometry_utils import intersection


def grab_contours_from_color_hsv(frame, color_lower, color_upper):
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    # construct a mask for the color, then perform
    # a series of dilations and erosions to remove any small blobs left in the mask
    mask = cv2.inRange(hsv, color_lower, color_upper)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)
    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    cnts = imutils.grab_contours(cnts)

    return cnts


def crop_non_white_object(im_thresh):
    # setting the color upper and lowwer of non white objects
    color_lower = (1, 1, 1)
    color_upper = (180, 230, 230)

    mask = cv2.inRange(im_thresh, color_lower, color_upper)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)

    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    cnts = sorted(cnts, key=cv2.contourArea)[:4]

    possible_squares = []
    for c in cnts:
        # get the bounding rect
        x, y, w, h = cv2.boundingRect(c)
        max_len = max([w, h])
        bounding_square = [[x, y], [x + max_len, y + max_len]]
        possible_squares.append(
            (intersection([x, y], [x, y + max_len], [x + max_len, y + max_len], [x + max_len, y]), bounding_square))

        # uncomment this line to draw a green rectangle to visualize the bounding rect
        cv2.rectangle(im_thresh, (x, y), (x + w, y + h), (0, 255, 0), 2)
        cv2.imshow("im tresh", im_thresh)

    def center_of_frame_sorter(x):
        center = im_thresh.shape[0] / 2
        return math.dist(x[0], [center, center])

    if len(possible_squares) == 0:
        return im_thresh
    # top frame is zero so no need to reverse!
    chosen_square = sorted(possible_squares, key=center_of_frame_sorter)[0]
    top_left, bottom_right = chosen_square[1][0], chosen_square[1][1]
    cropped = im_thresh[top_left[1]:bottom_right[1], top_left[0]:bottom_right[0]]
    resized = cv2.resize(cropped, (256, 256), interpolation=cv2.INTER_AREA)

    return resized
