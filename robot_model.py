import abc
import os
from abc import ABC

import config


class RobotModel(ABC):
    def __init__(self):
        self._controller = None
        self._disconnect = False
        self.SBRICK_MAC = config.SBRICK_MAC
        self.PROJECT_PATH = config.PROJECT_PATH

    # connect to the relevant components like nxt, sbrick
    @abc.abstractmethod
    def connect(self):
        pass

    # stop all operations with all motors
    @abc.abstractmethod
    def stop(self):
        pass

    @abc.abstractmethod
    def set_controller(self, controller):
        pass

    @abc.abstractmethod
    def _disconnect_from_sbrick(self):
        pass

    def exit(self):
        self.stop()
        self._disconnect = True
        self._disconnect_from_sbrick()

    # starting the sbrick server and waiting for commands to come
    def _start_sbrick_server(self):
        os.system(
            'python3 ' + self.PROJECT_PATH +
            '/Basket-Ball-Robot/sbrick_server.py --connect --broker-ip 127.0.0.1 --broker-port 1883  --sbrick-id ' +
            self.SBRICK_MAC)

