import threading
import nxt.usbsock
import nxt.bluesock
from nxt.sensor import *
from nxt.motor import *
from robot_model import RobotModel
import config
import time
from lib.sbrick_m2mipc import SbrickIpcClient
import os


class HolonomicRobotModel(RobotModel):

    def __init__(self):
        super().__init__()

        # sbrick settings
        self.GRABBER_MOTOR = '03'
        self.VERTICAL_MOTOR = '01'
        self.DEMON_MOTOR = '02'
        self.C_WISE = '00'
        self.CC_WISE = '01'
        self.MAX_POWER = 'ff'
        self.MID_POWER = 'Ff'
        self.LOW_POWER = 'B0'
        self.GRABBER_IN = self.CC_WISE
        self.GRABBER_OUT = self.C_WISE
        self.VERTICAL_DOWN = self.C_WISE
        self.VERTICAL_UP = self.CC_WISE
        self.THROW_OUT = self.CC_WISE
        self.SBRICK_NORMAL_WAITING_TIME = 0.5

    def holonomic_drive(self, axis0, axis1, left, right):
        pass

    def _connect_to_sbrick(self):
        threading.Thread(target=self._start_sbrick_server).start()
        self._sbrick_client = SbrickIpcClient(broker_ip='127.0.0.1', broker_port=1883)
        self._sbrick_client.connect()
        time.sleep(5)

    def _disconnect_from_sbrick(self):
        if self._sbrick_client is not None:
            self._sbrick_client.disconnect()

    # this demon is because the sbrick disconnect after a second without order
    # ordering the demon motor that is not connected and have no influence
    def _sbrick_demon(self):
        while not self._disconnect:
            self._sbrick_client.publish_drive(self.SBRICK_MAC, self.DEMON_MOTOR, self.C_WISE, self.MAX_POWER, 0.1)
            time.sleep(self.SBRICK_NORMAL_WAITING_TIME)

    # stop all operations with all motors
    def stop(self):
        self._sbrick_client.publish_stop(self.SBRICK_MAC,
                                         channel_list=[self.GRABBER_MOTOR, self.VERTICAL_MOTOR])

    def connect(self):
        self._connect_to_sbrick()

    def set_controller(self, controller):
        self._controller = controller
