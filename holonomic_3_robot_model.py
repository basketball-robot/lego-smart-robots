import math
import threading
import time

from holonomic_robot_model import HolonomicRobotModel
from utils.geometry_utils import get_angle_of_vector

MAX_POWER = 255


class Holonomic3RobotModel(HolonomicRobotModel):

    def __init__(self):
        super().__init__()
        # sbrick settings
        self.M1 = '03'
        self.M2 = '00'
        self.M3 = '01'

    def holonomic_drive(self, axis0, axis1, left, right):
        angle = get_angle_of_vector(axis0, axis1)
        abs_power = ((axis0**2 + axis1**2)**0.5) / (2**0.5)

        a_power = int(MAX_POWER*abs_power*math.sin(math.radians(90) + angle))
        b_power = int(MAX_POWER*abs_power*math.sin(math.radians(210) + angle))
        c_power = int(MAX_POWER*abs_power*math.sin(math.radians(330) + angle))

        if left != right:
            turn_power = 140
            if right:
                a_power = min(MAX_POWER, a_power + turn_power)
                b_power = min(MAX_POWER, b_power + turn_power)
                c_power = min(MAX_POWER, c_power + turn_power)
            if left:
                a = max(-MAX_POWER, a_power - turn_power)
                b = max(-MAX_POWER, b_power - turn_power)
                c = max(-MAX_POWER, c_power - turn_power)

        C_WISE = '00'
        CC_WISE = '01'
        a_direction = C_WISE
        b_direction = C_WISE
        c_direction = C_WISE
        if a_power < 0:
            a_direction = CC_WISE
        if b_power < 0:
            b_direction = CC_WISE
        if c_power < 0:
            c_direction = CC_WISE

        a_power_str = (hex(abs(a_power)) + '0')[2:4]
        b_power_str = (hex(abs(b_power)) + '0')[2:4]
        c_power_str = (hex(abs(c_power)) + '0')[2:4]

        # print(axis0, axis1)
        # print(x1_power_str, x2_power_str, y1_power_str, y2_power_str)
        # print('-----------------------')

        self._sbrick_client.publish_drive(self.SBRICK_MAC, self.M1, a_direction, a_power_str, 0.1)
        self._sbrick_client.publish_drive(self.SBRICK_MAC, self.M2, b_direction, b_power_str, 0.1)
        self._sbrick_client.publish_drive(self.SBRICK_MAC, self.M3, c_direction, c_power_str, 0.1)

    def _connect_to_sbrick(self):
        super(Holonomic3RobotModel, self)._connect_to_sbrick()
        threading.Thread(target=self._sbrick_demon).start()

    # stop all operations with all motors
    def stop(self, segmentation=False):
        self._sbrick_client.publish_stop(self.SBRICK_MAC, channel_list=[self.M1, self.M2, self.M3])
