import time
import random

from baketball_robot_controller import BasketballRobotController
from basketball_robot_model import BasketballRobotModel
from camera_model import CameraModel


class BlackMambaRobotController(BasketballRobotController):
    def __init__(self, black_mamba_robot_model, camera_model):
        super().__init__()
        self._camera_model: CameraModel = camera_model
        self._camera_model.set_controller(self)
        self._robot_model: BasketballRobotModel = black_mamba_robot_model
        self._robot_model.set_controller(self)
        self._frame_ball_ctr: int = 0
        self._frame_seek_ctr: int = 0
        self._frame_seek_contain_ctr: int = 0
        self._stop_ctr: int = 0
        self._frame_obstacle_ctr: int = 0
        self._in_operation: bool = False
        self._in_cleanup: bool = False
        self._ball_number: int = 1
        self._has_ball: bool = False
        self._state: str = ''

    def joystick_move(self, axis0, axis1):
        self._robot_model.drive(axis0, axis1, self._in_operation)

    def stop_operation(self):
        print('stopping operation')
        self._in_operation = False
        self._in_cleanup = False
        if self._camera_model:
            self._camera_model.set_tracking('all', False)
        self._robot_model.stop()

    def initiate(self):
        self._robot_model.connect()

    def pull_ball(self):
        self._robot_model.pull()

    def throw_ball(self):
        while not self._robot_model.update_thrower_voltage():
            print('cant get thrower voltage')
        self._robot_model.throw()

    def track_ball(self):
        self._in_operation = True
        if self._camera_model:
            self._camera_model.set_tracking('basket', False)
            self._robot_model.adjust_camera('ball')
            self._camera_model.set_tracking('ball', True)

    def track_basket(self):
        while not self._robot_model.update_thrower_voltage():
            print('cant get thrower voltage')

        self._in_operation = True
        if self._camera_model:
            self._robot_model.adjust_camera('basket')
            self._camera_model.set_tracking('ball', False)
            self._camera_model.set_tracking('basket', True)

    def autonomus(self):
        self._in_operation = True
        self._in_cleanup = True
        self._autonomous_flow()

    def _autonomous_flow(self):
        if self._camera_model:
            self._try_do_something()

    def _try_do_something(self):
        print('try do something')
        self._camera_model.set_tracking('all', False)
        if self._has_ball:
            print("go for basket")
            self._robot_model.adjust_camera('basket')
            self._camera_model.set_tracking('basket', True)
            return
        else:
            print("cant find ball moving to segmentation search")
            self._camera_model.set_tracking('segmentation_ball', True)

    def notify_has_ball(self, status):
        self._has_ball = status
        if self._in_cleanup:
            print('notify_has_ball autonomus')
            self._autonomous_flow()
        else:
            self.stop_operation()
            time.sleep(0.02)
            self.stop_operation()

    def notify_ball(self, ball_list: list):
        if len(ball_list) > 0:
            self._frame_seek_ctr = 0
            i = self._ball_number - 1
            self._frame_seek_ctr = 0
            if self._frame_ball_ctr > 3:
                self._robot_model.move_to_ball(ball_list[i][0][0], ball_list[i][0][1], ball_list[i][1])
                self._frame_ball_ctr = 0
            self._frame_ball_ctr += 1
        else:
            if self._frame_seek_contain_ctr > 100:
                if self._robot_model.is_contain_ball('red'):
                    self.notify_has_ball(True)
                self._frame_seek_contain_ctr = 0
            elif self._frame_seek_ctr > 5:
                if self._in_cleanup:
                    print('notify_ball autonomus')
                    self._autonomous_flow()
                self._frame_seek_ctr = 0
            self._frame_seek_ctr += 1
            self._frame_seek_contain_ctr += 1

    def notify_basket(self, x=None, y=None, distance=None, angle=None):
        if x is not None:
            if self._frame_ball_ctr > 4:
                self._robot_model.move_to_basket(x, y, distance, angle)
                self._frame_ball_ctr = 0
            self._frame_ball_ctr += 1
        else:
            if self._frame_seek_ctr > 10:
                try:
                    # while uncommented it is spinning
                    # self._robot_model.seek_for_basket()
                    self._robot_model.stop()
                except:
                    print('exception: restart camera')
                    self._camera_model.restart_loop()
                self._frame_seek_ctr = 0
            self._frame_seek_ctr += 1

    def notify_obstacle(self, obs):
        if not obs:
            if self._frame_obstacle_ctr > 0:
                self._robot_model.directed_drive(80, 80)
                self._frame_obstacle_ctr = 0
            self._frame_obstacle_ctr += 1
        else:
            if self._frame_obstacle_ctr > 0:
                print('this is BAD for continue')
                self._robot_model.stop()
                time.sleep(0.3)
                self._robot_model.turn(random.randint(1, 360))
                self._robot_model.adjust_camera('ball')
                self._frame_obstacle_ctr = 0
            self._frame_obstacle_ctr += 1

    def exit(self):
        self._camera_model.exit()
        self._robot_model.exit()
