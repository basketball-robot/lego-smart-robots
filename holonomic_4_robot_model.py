from holonomic_robot_model import HolonomicRobotModel

MAX_POWER = 255


class Holonomic4RobotModel(HolonomicRobotModel):

    def __init__(self):
        super().__init__()
        # sbrick settings
        self.M1 = '03'
        self.M2 = '00'
        self.M3 = '01'
        self.M4 = '02'

    def holonomic_drive(self, axis0, axis1, left, right):
        x1_power = int(MAX_POWER*axis0)
        x2_power = x1_power
        y1_power = int(MAX_POWER*axis1)
        y2_power = y1_power

        if left != right:
            turn_power = 140
            if right:
                x1_power = max(-MAX_POWER, x1_power - turn_power)
                x2_power = min(MAX_POWER, x2_power + turn_power)
                y1_power = min(MAX_POWER, y1_power + turn_power)
                y2_power = max(-MAX_POWER, y2_power - turn_power)
            if left:
                x1_power = min(MAX_POWER, x1_power + turn_power)
                x2_power = max(-MAX_POWER, x2_power - turn_power)
                y1_power = max(-MAX_POWER, y1_power - turn_power)
                y2_power = min(MAX_POWER, y2_power + turn_power)

        x1_power_str = (hex(abs(x1_power)) + '0')[2:4]
        x2_power_str = (hex(abs(x2_power)) + '0')[2:4]
        y1_power_str = (hex(abs(y1_power)) + '0')[2:4]
        y2_power_str = (hex(abs(y2_power)) + '0')[2:4]

        C_WISE = '00'
        CC_WISE = '01'
        x1_direction = CC_WISE
        x2_direction = C_WISE
        y1_direction = C_WISE
        y2_direction = CC_WISE
        if x1_power < 0:
            x1_direction = C_WISE
        if x2_power < 0:
            x2_direction = CC_WISE
        if y1_power < 0:
            y1_direction = CC_WISE
        if y2_power < 0:
            y2_direction = C_WISE

        # print(axis0, axis1)
        # print(x1_power_str, x2_power_str, y1_power_str, y2_power_str)
        # print('-----------------------')

        self._sbrick_client.publish_drive(self.SBRICK_MAC, self.M1, x1_direction, x1_power_str, 0.1)
        self._sbrick_client.publish_drive(self.SBRICK_MAC, self.M2, x2_direction, x2_power_str, 0.1)
        self._sbrick_client.publish_drive(self.SBRICK_MAC, self.M3, y1_direction, y1_power_str, 0.1)
        self._sbrick_client.publish_drive(self.SBRICK_MAC, self.M4, y2_direction, y2_power_str, 0.1)

    # stop all operations with all motors
    def stop(self, segmentation=False):
        self._sbrick_client.publish_stop(self.SBRICK_MAC, channel_list=[self.M1, self.M2, self.M3, self.M4])
