import os
import time
import pygame

import config
from baketball_robot_controller import BasketballRobotController
import abc
from abc import ABC

from brick_crawler_robot_controller import BrickCrawlerRobotController
from holonomic_robot_controller import HolonomicRobotController
from controller import Controller


class JoystickIO(ABC):
    def __init__(self, main_controller):
        self._done: bool = False
        self._r1_button: bool = False
        self._r2_button: bool = False
        self._l1_button: bool = False
        self._l2_button: bool = False

        # Used to manage how fast the screen updates.
        self._clock = pygame.time.Clock()
        self._main_controller: Controller = main_controller
        pygame.init()
        self._screen = None
        self._set_screen_window()
        self._set_joystick()
        self._main_controller.set_screen(self._screen)
        self._main_controller.initiate()

    def _set_screen_window(self):
        # Set the width and height of the screen (width, height).
        screen_width, screen_height = config.FRAME_WIDTH, config.FRAME_HEIGHT
        self._screen = pygame.display.set_mode((screen_width, screen_height))
        self._main_controller.set_screen(self._screen)
        pygame.display.set_caption("Robot Monitor")

    def _set_joystick(self):
        # Initialize the joysticks.
        pygame.joystick.init()

        joystick_exist = False
        while not joystick_exist:
            try:
                self._joystick = pygame.joystick.Joystick(0)
                joystick_exist = True
            except:
                print('no joystick has found, please connect joystick')
                time.sleep(2)
                pygame.joystick.init()

    def run_loop(self):
        A_BUTTON = config.A_BUTTON
        B_BUTTON = config.B_BUTTON
        X_BUTTON = config.X_BUTTON
        Y_BUTTON = config.Y_BUTTON
        L1_BUTTON = config.L1_BUTTON
        L2_BUTTON = config.L2_BUTTON
        R1_BUTTON = config.R1_BUTTON
        R2_BUTTON = config.R2_BUTTON
        BACK_BUTTON = config.BACK_BUTTON
        START_BUTTON = config.START_BUTTON

        # Main Program Loop
        while not self._done:
            # setting the pygame window to be on top others
            os.environ['SDL_VIDEO_WINDOW_POS'] = "0,0"
            # listen if the user did something
            for event in pygame.event.get():
                # if user clicked close
                if event.type == pygame.QUIT:
                    self._done = True
                # push
                elif event.type == pygame.JOYBUTTONDOWN:
                    if event.button == A_BUTTON:
                        self.a_button()
                    elif event.button == Y_BUTTON:
                        self.y_button()
                    elif event.button == X_BUTTON:
                        self.x_button()
                    elif event.button == B_BUTTON:
                        self.b_button()
                    elif event.button == START_BUTTON:
                        self.start_button()
                    elif event.button == R1_BUTTON:
                        self._r1_button = True
                        self.r1_button()
                    elif event.button == R2_BUTTON:
                        self._r2_button = True
                        self.r2_button()
                    elif event.button == L1_BUTTON:
                        self._l1_button = True
                        self.l1_button()
                    elif event.button == L2_BUTTON:
                        self._l2_button = True
                        self.l2_button()
                    elif event.button == BACK_BUTTON:
                        self.back_button()
                        self._done = True
                # release
                elif event.type == pygame.JOYBUTTONUP:
                    if event.button == R1_BUTTON:
                        self._r1_button = False
                    elif event.button == R2_BUTTON:
                        self._r2_button = False
                    elif event.button == L1_BUTTON:
                        self._l1_button = False
                    elif event.button == L2_BUTTON:
                        self._l2_button = False

            # get joystick axis
            axis0 = self._joystick.get_axis(0)
            axis1 = self._joystick.get_axis(1)

            # update the main controller about joystick moves
            self.move_joystick(axis0, -axis1)

            # update the screen with what we've drawn.
            pygame.display.flip()
            # Limit to 20 frames per second.
            self._clock.tick(20)
        pygame.quit()

    def move_joystick(self, axis_x, axis_y):
        pass

    def a_button(self):
        pass

    def b_button(self):
        pass

    def x_button(self):
        pass

    def y_button(self):
        pass

    def r1_button(self):
        pass

    def r2_button(self):
        pass

    def l1_button(self):
        pass

    def l2_button(self):
        pass

    def back_button(self):
        self._main_controller.exit()

    def start_button(self):
        pass


class BasketBallRobotIO(JoystickIO):
    def __init__(self, basket_ball_robot_controller):
        super().__init__(basket_ball_robot_controller)
        self._main_controller: BasketballRobotController = basket_ball_robot_controller

    def move_joystick(self, axis_x, axis_y):
        self._main_controller.joystick_move(axis_x, axis_y)

    def a_button(self):
        self._main_controller.track_ball()

    def b_button(self):
        self._main_controller.stop_operation()

    def x_button(self):
        self._main_controller.autonomus()

    def y_button(self):
        self._main_controller.track_basket()

    def r1_button(self):
        self._main_controller.pull_ball()

    def r2_button(self):
        self._main_controller.throw_ball()

    def start_button(self):
        self._main_controller.initiate()


class BrickCrawlerRobotIO(JoystickIO):
    def __init__(self, brick_crawler_controller):
        super().__init__(brick_crawler_controller)
        self._main_controller: BrickCrawlerRobotController = brick_crawler_controller

    def a_button(self):
        self._main_controller.horizontal_move(0)

    def b_button(self):
        self._main_controller.horizontal_move(1)

    def x_button(self):
        self._main_controller.horizontal_move(3)

    def y_button(self):
        self._main_controller.horizontal_move(2)


    def r1_button(self):
        self._main_controller.grab_brick()

    def r2_button(self):
        self._main_controller.drop_brick()

    def l1_button(self):
        self._main_controller.automation_brick_sorting()

    def l2_button(self):
        self._main_controller.recognize_frame()

    def start_button(self):
        self._main_controller.stop_operation()


class HolonomicRobotIO(JoystickIO):
    def __init__(self, holonomic_robot_controller):
        super().__init__(holonomic_robot_controller)
        self._main_controller: HolonomicRobotController = holonomic_robot_controller

    def move_joystick(self, axis_x, axis_y):
        self._main_controller.joystick_move(axis_x, axis_y, self._l1_button, self._r1_button)
