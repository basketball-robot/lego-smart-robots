from proxy_io import BasketBallRobotIO, BrickCrawlerRobotIO, HolonomicRobotIO

from camera_model import CameraModel

from brick_crawler_robot_controller import BrickCrawlerRobotController
from black_mamba_robot_controller import BlackMambaRobotController
from holonomic_robot_controller import HolonomicRobotController

from brick_crawler_robot_model import BrickCrawlerRobotModel
from black_mamba_robot_model import BlackMambaRobotModel
from holonomic_4_robot_model import Holonomic4RobotModel
from holonomic_3_robot_model import Holonomic3RobotModel


# start the server: sudo python3 HomeWorks/Basket-Ball-Robot/sbrick_server.py --connect --broker-ip 127.0.0.1 --broker-port 1883 --log-level debug --sbrick-id 00:07:80:d0:40:75 d0:cf:5e:53:7c:0f
# determine color: python3 PycharmProjects/Basket-Ball-Robot/utils/color_detector.py --filter HSV --webcam http://192.168.1.15:8080/video
# start hotspot: nmcli connection up Hotspot

def black_mamba():
    camera_model = CameraModel()
    robot_model = BlackMambaRobotModel()
    controller = BlackMambaRobotController(robot_model, camera_model)
    io = BasketBallRobotIO(controller)
    # starting the main loop that listen to joystick or other user input
    io.run_loop()


def crawler():
    camera_model = CameraModel()
    robot_model = BrickCrawlerRobotModel()
    controller = BrickCrawlerRobotController(robot_model, camera_model)
    io = BrickCrawlerRobotIO(controller)
    # starting the main loop that listen to joystick or other user input
    io.run_loop()


def holonomic4():
    robot_model = Holonomic4RobotModel()
    controller = HolonomicRobotController(robot_model)
    io = HolonomicRobotIO(controller)
    # starting the main loop that listen to joystick or other user input
    io.run_loop()

def holonomic3():
    robot_model = Holonomic3RobotModel()
    controller = HolonomicRobotController(robot_model)
    io = HolonomicRobotIO(controller)
    # starting the main loop that listen to joystick or other user input
    io.run_loop()

if __name__ == "__main__":
    # black_mamba()
    # crawler()
    # holonomic4()
    holonomic3()
