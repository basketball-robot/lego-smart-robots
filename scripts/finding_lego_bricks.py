import time
from collections import deque
import math

import cv2
import imutils
import numpy as np
import playsound
from imutils.video import WebcamVideoStream
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model

from utils import distance_calculator as dc

import config
from utils.geometry_utils import degree_with_floor, rectangle_get_top_left_down_right, intersection
from mutagen.mp3 import MP3

from utils.image_processing_utils import grab_contours_from_color_hsv, crop_non_white_object
from utils.ml_utils import predict_brick

audio = MP3("shutter.mp3")
wait_time = audio.info.length
video_src = config.VIDEO_SRC
vs = WebcamVideoStream(config.VIDEO_SRC).start()
time.sleep(2)


def test_model(original_frame):
    model = load_model('/home/kfir/PycharmProjects/Basket-Ball-Robot/ml_models/brick_model_4')
    frame = cv2.resize(original_frame, (32, 32))
    frame = frame.astype("float") / 255.0
    frame = img_to_array(frame)
    frame = np.expand_dims(frame, axis=0)
    # make predictions on the input image
    pred = model.predict(frame)
    pred = pred.argmax(axis=1)[0]
    # an index of zero is the 'parasitized' label while an index of
    # one is the 'uninfected' label
    label = "liftarm" if pred == 0 else "axel" if pred == 1 else "gear"
    color = (0, 0, 255) if pred == 0 else (0, 255, 0) if pred == 1 else (255, 0, 0)
    cv2.putText(original_frame, label, (3, 40), cv2.FONT_HERSHEY_SIMPLEX, 2,
                color, 2)

    cv2.imshow("Results", original_frame)
    return pred


# def super_cropper(im_thresh):
#     color_lower = (1, 1, 1)  # phone
#     color_upper = (180, 230, 230)  # phone
#     # hsv = cv2.cvtColor(im_thresh_color, cv2.COLOR_BGR2HSV)
#     mask = cv2.inRange(im_thresh, color_lower, color_upper)
#     mask = cv2.erode(mask, None, iterations=2)
#     mask = cv2.dilate(mask, None, iterations=2)
#
#     cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
#     cnts = imutils.grab_contours(cnts)
#     cnts = sorted(cnts, key=cv2.contourArea)[:2]
#
#     possible_squares = []
#     for c in cnts:
#         # get the bounding rect
#         x, y, w, h = cv2.boundingRect(c)
#         max_len = max([w, h])
#         bounding_square = [[x, y], [x + max_len, y + max_len]]
#         possible_squares.append(
#             (intersection([x, y], [x + max_len, y], [x, y + max_len], [x + max_len, y + max_len]), bounding_square))
#
#         # uncomment this line to draw a green rectangle to visualize the bounding rect
#         # cv2.rectangle(im_thresh, (x, y), (x + w, y + h), (0, 255, 0), 2)
#
#     def center_of_camera_sorter(x):
#         center = im_thresh.shape[0] / 2
#         return math.dist(x[0], [center, center])
#
#     # top frame is zero so no need to reverse!
#     chosen_square = sorted(possible_squares, key=center_of_camera_sorter)[0]
#     top_left, bottom_right = chosen_square[1][0], chosen_square[1][1]
#     cropped_2 = im_thresh[top_left[1]:bottom_right[1], top_left[0]:bottom_right[0]]
#     resized_2 = cv2.resize(cropped_2, (256, 256), interpolation=cv2.INTER_AREA)
#     return resized_2


while True:
    try:
        # uncomment this line for playing sound of shutter and it blocks the code for 2 seconds
        # so there is enough time to relocate another part for capture
        # playsound.playsound('shutter.mp3', True)

        # slowing it a little for humans understanding
        time.sleep(0.1)

        frame = vs.read()
        if frame is None:
            break

        # finding white color enclosing rect for detecting the part inside of it
        color_lower = (0, 0, 150)
        color_upper = (255, 92, 255)

        cnts = grab_contours_from_color_hsv(frame, color_lower, color_upper)

        cnts = sorted(cnts, key=cv2.contourArea, reverse=True)[:2]
        arr_of_shapes = []
        for ap in cnts:
            peri = cv2.arcLength(ap, True)
            arr_of_shapes.append(cv2.approxPolyDP(ap, 0.06 * peri, True))
        approx = [i for i in arr_of_shapes if len(i) == 4]

        def func1(x):
            return x[0][0][1]

        approx = sorted(approx, key=cv2.contourArea)[:2]
        # approx = sorted(approx, key=func2)[:1]
        for ap in approx:
            if len(ap) != 4:
                continue

            x_max = max([ap[i][0][0] for i in range(4)])
            x_min = min([ap[i][0][0] for i in range(4)])
            approx_width = int(abs(x_max - x_min))

            if approx_width < 100:
                continue

            # cropping according to enclosing white rect
            top_left, bottom_right = rectangle_get_top_left_down_right([ap[i][0] for i in range(4)])
            cropped = frame[top_left[1]:bottom_right[1], top_left[0]:bottom_right[0]]
            resized = cv2.resize(cropped, (296, 296), interpolation=cv2.INTER_AREA)
            cv2.imshow('cropped first', resized)
            # cropping the frames
            cropped_again = resized[20:276, 20:276]

            # masking the white to be real white
            im_color = cropped_again
            im_gray = cv2.cvtColor(im_color, cv2.COLOR_BGR2GRAY)
            _, mask = cv2.threshold(im_gray, thresh=180, maxval=255, type=cv2.THRESH_BINARY)
            mask3 = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)
            im_thresh_color = cv2.bitwise_or(im_color, mask3)
            cropped_super = crop_non_white_object(im_thresh_color)
            pred = predict_brick(cropped_super)

            # uncomment this line for saving data
            # print(cv2.imwrite('data/' + time.strftime("%Y%m%d-%H%M%S") + '.png', cropped_super))

            label = "liftarm" if pred == 0 else "axel" if pred == 1 else "gear"
            color = (0, 0, 255) if pred == 0 else (0, 255, 0) if pred == 1 else (255, 0, 0)
            cv2.putText(cropped_super, label, (3, 40), cv2.FONT_HERSHEY_SIMPLEX, 2,
                        color, 2)

            cv2.imshow("Results", cropped_super)

        cv2.waitKey(1)
        cv2.imshow("Frame", frame)
    except:
        {}
vs.release()
cv2.destroyAllWindows()
