import pygame
from pygame.locals import *
import cv2
import numpy
import config

camera_index = 0
camera = cv2.VideoCapture(config.VIDEO_SRC)

retval, frame = camera.read()

# This shows an image weirdly...
screen_width, screen_height = 640, 480
screen = pygame.display.set_mode((screen_width, screen_height))


def getCamFrame(camera):
    retval, frame = camera.read()
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    frame = pygame.surfarray.make_surface(frame)  # I think the color error lies in this line?
    return frame


def blitCamFrame(frame, screen):
    screen.blit(frame, (0, 0))
    return screen


screen.fill(0)  # set pygame screen to black
frame = getCamFrame(camera)
screen = blitCamFrame(frame, screen)
pygame.display.flip()

running = True
while running:
    for event in pygame.event.get():  # process events since last loop cycle
        if event.type == KEYDOWN:
            running = False
pygame.quit()
cv2.destroyAllWindows()
