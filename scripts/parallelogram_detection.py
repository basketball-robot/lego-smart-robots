import math
import time
from collections import deque

import cv2
import imutils

import config
from utils import distance_calculator as dc
from utils.geometry_utils import is_normal_shape
from utils.geometry_utils import intersection

video_src = config.VIDEO_SRC
vs = cv2.VideoCapture(video_src)
color_lower = (0,81,184)  # phone
color_upper = (184, 195, 255)  # phone
pts = deque(maxlen=32)
while True:
    while (True):
        prev_time = time.time()
        ref = vs.grab()
        if (time.time() - prev_time) > 0.005:  # something around 33 FPS
            break
    frame = vs.read()
    frame = frame[1]
    frame = cv2.flip(frame, 0)
    if frame is None:
        break
    # frame = imutils.resize(frame, width=600)
    frame = cv2.flip(frame, 0)
    color_lower = (107, 139, 60)  # phone
    color_upper = (123, 255, 255)  # phone
    blurred = cv2.GaussianBlur(frame, (11, 11), 0)
    hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
    # construct a mask for the color, then perform
    # a series of dilations and erosions to remove any small blobs left in the mask
    mask = cv2.inRange(hsv, color_lower, color_upper)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)
    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)

    cnts = sorted(cnts, key=cv2.contourArea, reverse=True)[:1]
    # loop over our contours witch is only one
    for c in cnts:
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.06 * peri, True)
        if len(approx) == 4 and is_normal_shape(approx[0][0], approx[1][0], approx[2][0], approx[3][0]):  # and ratio(approx):
            if 1:
                x_max = max([approx[i][0][0] for i in range(4)])
                x_min = min([approx[i][0][0] for i in range(4)])
                width = int(abs(x_max - x_min))
                if width > 20:
                    cv2.drawContours(frame, [approx], -1, (0, 255, 0), 3)
                    new_center = intersection(approx[0][0], approx[1][0], approx[2][0], approx[3][0])
                    ONE_PLUS_FOCAL = 568
                    PAPER_WIDTH = 18.3
                    YETER_distance = dc.distance_to_camera(PAPER_WIDTH, ONE_PLUS_FOCAL, width)
                    BASKET_HEIGHT = 32
                    inner = YETER_distance * YETER_distance - BASKET_HEIGHT * BASKET_HEIGHT
                    if inner <= 0:
                        distance = -1
                    else:
                        distance = math.sqrt(YETER_distance * YETER_distance - BASKET_HEIGHT * BASKET_HEIGHT)
                    cv2.putText(frame, "%.2f_cm" % distance,
                                (0, 60), cv2.FONT_HERSHEY_SIMPLEX,
                                2.0, (0, 255, 255), 3)

                    center = cx, cy = int((approx[0][0][0] + approx[2][0][0]) / 2), int(
                        (approx[1][0][1] + approx[3][0][1]) / 2)
                    cv2.circle(frame, center, 5, (0, 255, 0), -1)
                    print(new_center)
                    cv2.circle(frame, new_center, 5, (0, 0, 250), -1)

    # cnts = sorted(cnts, key=cv2.contourArea, reverse=True)[:1]
    # # loop over our contours
    # for c in cnts:
    #     peri = cv2.arcLength(c, True)
    #     approx = cv2.approxPolyDP(c, 0.06 * peri, True)
    #     print(len(approx))
    #     if len(approx) == 4:  # and ratio(approx):
    #         cv2.drawContours(frame, [approx], -1, (0, 255, 0), 3)



    cv2.waitKey(1)
    cv2.imshow("Frame", frame)
vs.release()
cv2.destroyAllWindows()
