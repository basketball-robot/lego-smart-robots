import time

import imutils
import cv2
import argparse

# initialize the known distance from the camera to the object, which
# in this case is 24 cm
import numpy as np

KNOWN_DISTANCE = 27.5
# initialize the known object width, which in this case, the piece of
# paper is 30 cm wide
KNOWN_HEIGHT = 23.0
# load the furst image that contains an object that is KNOWN TO BE 2 feet
# from our camera, then find the paper marker in the image, and initialize
# the focal length


def get_arguments():
    ap = argparse.ArgumentParser()
    ap.add_argument('-w', '--webcam', required=False,
                    help='Use webcam')
    args = vars(ap.parse_args())

    return args


def main():
    args = get_arguments()

    video_src = cv2.VideoCapture(args['webcam'])
    time.sleep(1)
    while True:
        ret, frame = video_src.read()
        if not ret:
            break
        # convert the image to grayscale, blur it, and detect edges
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (5, 5), 0)
        edged = cv2.Canny(gray, 35, 125)
        # find the contours in the edged image and keep the largest one;
        # we'll assume that this is our piece of paper in the image
        cnts = cv2.findContours(edged.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
        c = max(cnts, key=cv2.contourArea)
        # compute the bounding box of the of the paper region and return it
        marker = cv2.minAreaRect(c)
        focalLength = (480 * KNOWN_DISTANCE) / KNOWN_HEIGHT
        # draw a bounding box around the image and display it
        box = cv2.boxPoints(marker)
        box = np.int0(box)
        cv2.drawContours(frame, [box], -1, (0, 255, 0), 2)
        cv2.putText(frame, "%.2f" % focalLength,
                    (frame.shape[1] - 200, frame.shape[0] - 20), cv2.FONT_HERSHEY_SIMPLEX,
                    2.0, (0, 255, 0), 3)
        cv2.imshow("frame", frame)
        if cv2.waitKey(1) & 0xFF is ord('q'):
            break

if __name__ == '__main__':
    main()