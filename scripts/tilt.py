import nxt.bluesock
from nxt import Motor
from nxt.sensor import *
import time
from nxt.motor import *

# _nxt = nxt.bluesock.BlueSock().connect()
NXT_MAC = '00:16:53:0A:9B:92'
# NXT2_MAC = '00:16:53:1A:30:E0'
_nxt = nxt.bluesock.BlueSock(NXT_MAC).connect()
_s_accel = MSACCL(_nxt, PORT_1)
m_camera = Motor(_nxt, PORT_A)
for i in range(33):
    tilt = _s_accel.get_tilt('z')
    d = 360 * ((tilt + 24) / 4 - 11) / 3
    sign = -1
    if d < 0:
        d = -d
        sign = 1
    print(tilt,d)
    m_camera.turn(sign*127, int(d))
    time.sleep(1.0)
# print(2)
# print(2)
# print(2)
# print(2)
# print(2)
# print(2)
# print(2)
# _s_color = HTColorv2(_nxt, PORT_3)
# for i in range(100):
#     color = _s_color.get_active_color()
#     print(color.red)
#     time.sleep(0.3)