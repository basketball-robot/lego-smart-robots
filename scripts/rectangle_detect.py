import time
from collections import deque
import math

import cv2
import imutils
import numpy as np

from utils import distance_calculator as dc

import config
from utils.geometry_utils import degree_with_floor

video_src = config.VIDEO_SRC
vs = cv2.VideoCapture(video_src)
# color_lower = (0,81,184)  # phone
# color_upper = (184, 195, 255)  # phone
pts = deque(maxlen=32)
while True:
    while (True):
        prev_time = time.time()
        ref = vs.grab()
        if (time.time() - prev_time) > 0.005:  # something around 33 FPS
            break
    frame = vs.read()
    frame = frame[1]
    frame = cv2.flip(frame, 0)
    if frame is None:
        break
    # frame = imutils.resize(frame, width=600)
    color_lower = (100, 100, 50)  # phone
    color_upper = (123, 255, 255)  # phone
    blurred = cv2.GaussianBlur(frame, (11, 11), 0)
    hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
    # construct a mask for the color, then perform
    # a series of dilations and erosions to remove any small blobs left in the mask
    mask = cv2.inRange(hsv, color_lower, color_upper)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)
    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    cnts = imutils.grab_contours(cnts)

    cnts = sorted(cnts, key=cv2.contourArea, reverse=True)[:5]
    # loop over our contours witch is only one
    is_there_basket = False
    arr_of_shapes = []
    for c in cnts:
        peri = cv2.arcLength(c, True)
        # approx = cv2.approxPolyDP(c, 0.06 * peri, True)
        arr_of_shapes.append(cv2.approxPolyDP(c, 0.06 * peri, True))
    approx = [i for i in arr_of_shapes if len(i) == 4 and i[0][0][1] < 300 and dc.area_of_rectangle(i) > 500]
    # print(len(approx))
    # for i in approx:
    #     print(dc.area_of_rectangle(i))
    # time.sleep(0.2)

    def func1(x):
        return x[0][0][1]


    def func2(x):
        width, height = dc.dimension_of_rectangle(x)
        return (width / height - 19 / 9) * (width / height - 19 / 9)


    approx = sorted(approx, key=func1)[:2]
    # approx = sorted(approx, key=func2)[:1]
    for approx in approx:
        # print(approx)
        if len(approx) == 4:  # and ratio(approx):
            x_max = max([approx[i][0][0] for i in range(4)])
            x_min = min([approx[i][0][0] for i in range(4)])
            width = int(abs(x_max - x_min))
            if width > 20:
                print(width)
                degree = degree_with_floor(approx[0][0], approx[1][0], approx[2][0], approx[3][0])
                width = width/(math.cos(np.deg2rad(degree*2.5)))
                print(width)
                is_there_basket = True
                cv2.drawContours(frame, [approx], -1, (0, 255, 0), 3)

                ONE_PLUS_FOCAL = 568
                PAPER_WIDTH = 18.3
                YETER_distance = dc.distance_to_camera(PAPER_WIDTH, ONE_PLUS_FOCAL, width)
                CAMERA_HEIGHT = 15
                BASKET_HEIGHT = 32 - CAMERA_HEIGHT
                inner = YETER_distance * YETER_distance - BASKET_HEIGHT * BASKET_HEIGHT
                if inner <= 0:
                    distance = -1
                else:
                    distance = math.sqrt(YETER_distance * YETER_distance - BASKET_HEIGHT * BASKET_HEIGHT)
                cv2.putText(frame, "%.2f_cm" % distance,
                            (0, 60), cv2.FONT_HERSHEY_SIMPLEX,
                            2.0, (0, 255, 255), 3)
                center = cx, cy = int((approx[0][0][0] + approx[2][0][0]) / 2), int(
                    (approx[1][0][1] + approx[3][0][1]) / 2)
                cv2.circle(frame, center, 5, (0, 255, 0), -1)

                cv2.putText(frame, "%.2f_deg" % degree,
                            (0, 160), cv2.FONT_HERSHEY_SIMPLEX,
                            2.0, (0, 255, 0), 3)
                cv2.putText(frame, "%.2f_center" % cx,
                            (0, 260), cv2.FONT_HERSHEY_SIMPLEX,
                            2.0, (0, 255, 0), 3)

    cv2.waitKey(1)
    cv2.imshow("Frame", frame)
vs.release()
cv2.destroyAllWindows()
