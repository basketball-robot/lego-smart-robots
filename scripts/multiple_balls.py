import time
from collections import deque

import cv2
import imutils
import numpy as np

video_src = 'http://192.168.1.15:8080/video'
# video_src = 'http://10.42.0.233:8080/video'  # lenovo 480
vs = cv2.VideoCapture(video_src)
# color_lower = (0,81,184)  # phone
# color_upper = (184, 195, 255)  # phone
red_lower = (0, 143, 0)  # phone
red_upper = (10, 255, 255)  # phone

pts = deque(maxlen=32)
while True:
    while (True):
        prev_time = time.time()
        ref = vs.grab()
        if (time.time() - prev_time) > 0.005:  # something around 33 FPS
            break
    frame = vs.read()
    frame = frame[1]
    if frame is None:
        break
    # frame = imutils.resize(frame, width=600)
    frame = cv2.flip(frame, 0)
    blurred = cv2.GaussianBlur(frame, (15, 15), 0)
    cv2.imshow("Frameb", blurred)

    hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, red_lower, red_upper)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)

    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    ball_cnt_list = []
    ball_list = []
    counter = 1
    sorted(cnts, key=cv2.contourArea)
    for contour in cnts:
        approx = cv2.approxPolyDP(contour, 0.01 * cv2.arcLength(contour, True), True)
        area = cv2.contourArea(contour)
        (cx, cy), radius = cv2.minEnclosingCircle(contour)
        center = (int(cx), int(cy))
        circleArea = radius * radius * np.pi
        print(circleArea, area)
        cv2.drawContours(frame, [contour], 0, (220, 152, 91), -1)
        if (len(approx) > 8) and (circleArea * 0.65 < area < circleArea * 1.2):
            cv2.circle(frame, center, int(radius), (0, 255, 255), 2)
            ball_cnt_list.append(contour)
            ball_list.append((center, int(radius), counter))
            counter += 1
    for ball in ball_list:
        cv2.putText(frame, "%.f" % ball[2], ball[0], cv2.FONT_HERSHEY_SIMPLEX, 2.0, (0, 255, 255), 3)
    cv2.waitKey(1)
    cv2.imshow("Frame", frame)
vs.release()
cv2.destroyAllWindows()
