import time
from collections import deque

import cv2
import imutils
import numpy as np
import argparse
import glob
from matplotlib import pyplot as plt

import config

video_src = config.VIDEO_SRC

# video_src = 'http://192.168.1.15:8080/video'
vs = cv2.VideoCapture(video_src)
while True:
    while (True):
        prev_time = time.time()
        ref = vs.grab()
        if (time.time() - prev_time) > 0.005:  # something around 33 FPS
            break
    frame = vs.read()
    frame = frame[1]
    frame = cv2.flip(frame, 0)
    if frame is None:
        break


    # varsion 1
    StepSize = 8
    EdgeArray = []

    img = frame
    image = frame
    # Create ROI coordinates
    topLeft = (0,300)
    bottomRight = (720,480)
    x, y = topLeft[0], topLeft[1]
    w, h = bottomRight[0] - topLeft[0], bottomRight[1] - topLeft[1]

    # Grab ROI with Numpy slicing and blur
    ROI = image[y:y + h, x:x + w]
    blur = cv2.GaussianBlur(ROI, (51, 51), 0)

    # Insert ROI back into image
    # image[y:y + h, x:x + w] = blur  # insert blur close to camera
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    kernel_size = 5
    blur_gray = cv2.GaussianBlur(gray, (kernel_size, kernel_size), 0)
    blur_gray = cv2.medianBlur(frame, 17)
    low_threshold = 20
    high_threshold = 50
    imgEdge = cv2.Canny(blur_gray, low_threshold, high_threshold)

    imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)  # convert img to grayscale and store result in imgGray
    imgGray = cv2.bilateralFilter(imgGray, 9, 30, 30)  # blur the image slightly to remove noise
    imgEdge = cv2.Canny(imgGray, 50, 100)  # edge detection

    # https://www.programmersought.com/article/7671791785/
    blur_gray = cv2.medianBlur(frame, 35)
    gray = cv2.cvtColor(blur_gray, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(
        gray, 230, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    # Eliminate noise
    kernel = np.ones((3, 3), np.uint8)
    opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=3)
    #
    sure_bg = cv2.dilate(opening, kernel, iterations=3)
    #
    imgEdge = imutils.auto_canny(sure_bg)


    # imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)  # convert img to grayscale and store result in imgGray
    # cv2.imshow("Frame2", imgGray)
    # imgGray = cv2.bilateralFilter(imgGray, 9, 30, 30)  # blur the image slightly to remove noise
    # imgEdge = cv2.Canny(imgGray, 50, 100)  # edge detection
    # cv2.imshow("Frame3", imgGray)
    cv2.imshow("Frame4", imgEdge)
    imagewidth = imgEdge.shape[1] - 1
    imageheight = imgEdge.shape[0] - 1

    for j in range(0, imagewidth, StepSize):  # for the width of image array
        for i in range(imageheight - 5, 0, -1):  # step through every pixel in height of array from bottom to top
            # Ignore first couple of pixels as may trigger due to undistort
            if imgEdge.item(i, j) == 255:  # check to see if the pixel is white which indicates an edge has been found
                if i > imageheight * 0.75 and imagewidth * 2 / 3 > j > imagewidth / 3:
                    print((j, i), 'this is BAD for continue')
                EdgeArray.append((j, i))  # if it is, add x,y coordinates to ObstacleArray
                break  # if white pixel is found, skip rest of pixels in column
        else:  # no white pixel found
            EdgeArray.append(
                (j, 0))  # if nothing found, assume no obstacle. Set pixel position way off the screen to indicate
            # no obstacle detected

    for x in range(len(EdgeArray) - 1):  # draw lines between points in ObstacleArray
        cv2.line(img, EdgeArray[x], EdgeArray[x + 1], (0, 255, 0), 1)
    for x in range(len(EdgeArray)):  # draw lines from bottom of the screen to points in ObstacleArray
        cv2.line(img, (x * StepSize, imageheight), EdgeArray[x], (0, 255, 0), 1)


    cv2.waitKey(1)
    cv2.imshow("Frame", img)
vs.release()
cv2.destroyAllWindows()
