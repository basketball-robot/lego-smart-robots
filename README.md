# Lego-Smart-Robots
This is a python based interface for control LEGO NXT and SBrick based robots with additional view sensor such as camera.

### Video Link (project explanation in Hebrew)
https://www.youtube.com/watch?v=AO-IifibJuc

## Aarchitecture overview:
![Alt text](architecture.png)

## Requirements
* Python 3.8 (did not test on prev versions)
* mosquitto
* mosquitto-clients
* paho-mqtt
* pyuv
* bluepy==1.3.0
* opencv-python
* libbluetooth-dev
* pybluez
* [nxt-python --version3](https://github.com/Eelviny/nxt-python) 
* pynput
* numpy
* matplotlib
* pygame
* imutils
* tensorflow
* PyUSB (for connecting nxt via usb)

## Usage

### Preparations:
1. Create your favourite lego model. Driving and camera adjast via NXT and the rest via SBrick.
2. prepare a wifi camera and a joystick.
3. Clone to master.

### Configurations:
#### Frame size:
You need to decide what is your frame size in pixels (eg. 720X480) then copy the values to the config.py file.

#### Camera Focal Length
A simple way to detect the focal length is to adjust the camera to fit object with known size
and to calculate this equation: 
```text
focal_length = (PIXELS_OF_OBJECT_SIZE * KNOWN_DISTANCE) / KNOWN_OBJECT_SIZE
```
For example if you use 480 pixels for the height  of the frame and take an A4 page (29.5 cm) as the object,
 measure the distance to camera when the page fits the height of the frame:
```text
focal_length = (480 * DISTANCE_TO_CAMERA) / 29.5
```
Then copy the value to the config.py file.

#### Colors
Configure the HSV color bounds of the ball and the rectangle of the basket or other rectangle you want to track.
You can use HSV detector util. To open it simply run:
```bash
$ python3 Path/to/project/Basket-Ball-Robot/utils/color_detector.py -f HSV -w http://your.video.src.url
```
Then copy the values to the config.py file in the appropriate place.

#### NXT and SBrick MAC address:
##### Via USB:
No need to get any man address, it auto detect.
But the program should run with root privileges.

##### Via Bluetooth:
To find the mac address first open terminal and type:
```bash
$ bluetoothctl
[bluetooth]# agent on
[bluetooth]# scan on
```
Then wait few seconds and open the SBrick or NXT, you will see new devices in the scan.
Pair:
```bash
[bluetooth]# pair mac_address (eg. 10:C2:5A:21:17:3A)
[bluetooth]# trust mac_address
```
Now your SBrick and NXT are paired and trusted and ready for connections.
Finally copy the mac address to the config.py file.

Note: when pairing NXT the nxt will ask to generate password, you need to pas it to the terminal.
#### (optional) Prepare The CNN Model
For applying the CNN model in the project check the config file.
If you want to use the CNN model for diversifying the floor and the bounds (in this project) or others in yours,
you need to collect data for your environment and separate it to categories.
cd to project dir:
```bash
$ cd path/to/project/Basket-Ball-Robot/ml_models
``` 
collect the data and separate it to different calasses, open your camera stream then run the script,
while capturing aim for all of the options (eg. floor, bounds, above). then copy the relevant pictures to different
directories (eg. floor, bounds, above). To run the script for collecting the data to 'data' dir: 
```bash
$ python3 collecting_data_from_camera.py -w http://your.video.src.url
```
To train the model with the data (pass the path to the dir that contains the classes dirs (in the project 'data'):
```bash
$ python3 train.py -d data -n yourNewModelName
```
To test the trained model with your real data start camera stream and:
```bash
$ python3 test_model.py -m yourNewModelName
```
You will see on the screen the stream and a label of the frame (eg. floor, bounds, above).
For using your trained model in the project copy the name to config.py.