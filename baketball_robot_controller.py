import abc
from controller import Controller


class BasketballRobotController(Controller):

    @abc.abstractmethod
    def joystick_move(self, *args):
        pass

    @abc.abstractmethod
    def stop_operation(self):
        pass

    @abc.abstractmethod
    def initiate(self):
        pass

    @abc.abstractmethod
    def pull_ball(self):
        pass

    @abc.abstractmethod
    def throw_ball(self):
        pass

    @abc.abstractmethod
    def track_ball(self):
        pass

    @abc.abstractmethod
    def track_basket(self):
        pass

    @abc.abstractmethod
    def notify_has_ball(self, *args):
        pass

    @abc.abstractmethod
    def notify_ball(self, *args):
        pass

    @abc.abstractmethod
    def notify_basket(self, *args):
        pass

    @abc.abstractmethod
    def notify_obstacle(self, *args):
        pass

    @abc.abstractmethod
    def autonomus(self):
        pass

    @abc.abstractmethod
    def exit(self):
        pass
