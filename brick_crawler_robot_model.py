import threading
import nxt.usbsock
import nxt.bluesock
from nxt.sensor import *
from nxt.motor import *
from robot_model import RobotModel
import config
import time
from lib.sbrick_m2mipc import SbrickIpcClient
import os


class BrickCrawlerRobotModel(RobotModel):

    def __init__(self):
        super().__init__()
        self.VERTICAL_TIME_UP = 1.5
        self.VERTICAL_DOWN_TIME = 1
        self.GRAB_TIME = 0.7
        self.FRAME_WIDTH = config.FRAME_WIDTH

        # sbrick settings
        self.GRABBER_MOTOR = '03'
        self.VERTICAL_MOTOR = '01'
        self.DEMON_MOTOR = '00'
        self.C_WISE = '00'
        self.CC_WISE = '01'
        self.MAX_POWER = 'ff'
        self.MID_POWER = 'Ff'
        self.LOW_POWER = 'B0'
        self.GRABBER_IN = self.CC_WISE
        self.GRABBER_OUT = self.C_WISE
        self.VERTICAL_DOWN = self.C_WISE
        self.VERTICAL_UP = self.CC_WISE
        self.THROW_OUT = self.CC_WISE
        self.SBRICK_NORMAL_WAITING_TIME = 0.5

        # nxt settings
        self.NXT_MAX_POWER = 127
        self._sbrick_client = None
        self._disconnect = False
        self._m_horizontal = None
        self._s_distance = None

    def horizontal_move(self, pos):
        positions_ultrasonic = {0: 6,
                                1: 17,
                                2: 22,
                                3: 27}
        reached_correct_pos = False
        while not reached_correct_pos:
            distance = self._s_distance.get_distance()
            if positions_ultrasonic[pos] > distance:
                self._m_horizontal.run(-80)
            elif positions_ultrasonic[pos] == distance:
                self._m_horizontal.brake()
                self._m_horizontal.idle()
                reached_correct_pos = True
            else:
                self._m_horizontal.run(80)

    def _connect_to_sbrick(self):
        threading.Thread(target=self._start_sbrick_server).start()
        self._sbrick_client = SbrickIpcClient(broker_ip='127.0.0.1', broker_port=1883)
        self._sbrick_client.connect()
        time.sleep(5)

        threading.Thread(target=self._sbrick_demon).start()

    def _connect_to_nxt(self):
        success = False
        while not success:
            try:
                self._nxt = nxt.locator.find_one_brick(debug=True)
                success = True
            except:
                print('cant connect to nxt, reconnecting')

        self._m_horizontal = Motor(self._nxt, PORT_A)
        self._s_distance = MSDIST(self._nxt, PORT_1)

    def _disconnect_from_sbrick(self):
        if self._sbrick_client is not None:
            self._sbrick_client.disconnect()

    # this demon is because the sbrick disconnect after a second without order
    # ordering the demon motor that is not connected and have no influence
    def _sbrick_demon(self):
        while not self._disconnect:
            self._sbrick_client.publish_drive(self.SBRICK_MAC, self.DEMON_MOTOR, self.C_WISE, self.MAX_POWER, 0.1)
            time.sleep(self.SBRICK_NORMAL_WAITING_TIME)

    # stop all operations with all motors
    def stop(self, segmentation=False):
        self._m_horizontal.idle()
        self._sbrick_client.publish_stop(self.SBRICK_MAC,
                                         channel_list=[self.GRABBER_MOTOR, self.VERTICAL_MOTOR])

    def connect(self):
        self._connect_to_nxt()
        time.sleep(2)
        self._connect_to_sbrick()

    def set_controller(self, controller):
        self._controller = controller

    def reset_horizontal_pos(self):
        self.horizontal_move(0)

    def grab_brick(self):
        self._sbrick_client.publish_drive(self.SBRICK_MAC, self.VERTICAL_MOTOR, self.VERTICAL_DOWN, self.MAX_POWER, self.VERTICAL_TIME_UP)
        time.sleep(self.VERTICAL_TIME_UP)
        self._sbrick_client.publish_drive(self.SBRICK_MAC, self.GRABBER_MOTOR, self.GRABBER_IN, self.MAX_POWER, 1.5 * self.VERTICAL_TIME_UP)
        time.sleep(self.VERTICAL_TIME_UP)
        self._sbrick_client.publish_drive(self.SBRICK_MAC, self.VERTICAL_MOTOR, self.VERTICAL_UP, self.MAX_POWER,
                                          self.VERTICAL_TIME_UP+0.1)
        time.sleep(self.VERTICAL_TIME_UP)

    def grab_brick_from_white_board(self):
        self._sbrick_client.publish_drive(self.SBRICK_MAC, self.VERTICAL_MOTOR, self.VERTICAL_DOWN, self.MAX_POWER, self.VERTICAL_DOWN_TIME+0.6)
        time.sleep(self.VERTICAL_DOWN_TIME)
        self._sbrick_client.publish_drive(self.SBRICK_MAC, self.GRABBER_MOTOR, self.GRABBER_IN, self.MAX_POWER, 1.5 * self.VERTICAL_TIME_UP)
        time.sleep(self.VERTICAL_TIME_UP)
        self._sbrick_client.publish_drive(self.SBRICK_MAC, self.VERTICAL_MOTOR, self.VERTICAL_UP, self.MAX_POWER,
                                          self.VERTICAL_TIME_UP+0.1)
        time.sleep(self.VERTICAL_DOWN_TIME)

    def drop_brick(self):
        self._sbrick_client.publish_drive(self.SBRICK_MAC, self.VERTICAL_MOTOR, self.VERTICAL_DOWN, self.MAX_POWER, self.VERTICAL_DOWN_TIME)
        time.sleep(self.VERTICAL_DOWN_TIME)
        self._sbrick_client.publish_drive(self.SBRICK_MAC, self.GRABBER_MOTOR, self.GRABBER_OUT, self.MAX_POWER, self.GRAB_TIME)
        time.sleep(self.GRAB_TIME)
        self._sbrick_client.publish_drive(self.SBRICK_MAC, self.VERTICAL_MOTOR, self.VERTICAL_UP, self.MAX_POWER,
                                          self.VERTICAL_DOWN_TIME+0.1)
        time.sleep(self.VERTICAL_DOWN_TIME)
