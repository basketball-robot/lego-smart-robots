import json
import threading

import nxt.bluesock
from nxt.sensor import *
from nxt.motor import *

from basketball_robot_model import BasketballRobotModel

import config
import time
from lib.sbrick_m2mipc import SbrickIpcClient
import os


class BlackMambaRobotModel(BasketballRobotModel):
    def __init__(self):
        super().__init__()
        self.FRAME_WIDTH = config.FRAME_WIDTH

        # sbrick settings
        self.DRIBLER_MOTOR = '03'
        self.PULLER_MOTOR = '02'
        self.THROW_MOTOR = '01'
        self.DEMON_MOTOR = '00'
        self.EXEC_TIME = 0.5
        self.DIRECT_TIME = 0.5
        self.SUPER_DIRECT_TIME = 1
        self.THROW_WARMUP_TIME = 1.3
        self.SEEK_TIME = 0.2
        self.DRIBLER_IN = self.C_WISE
        self.DRIBLER_OUT = self.CC_WISE
        self.PULLER_IN = self.C_WISE
        self.PULLER_OUT = self.CC_WISE
        self.THROW_OUT = self.CC_WISE

        # nxt settings
        self.NXT_MAX_POWER = 127
        self._direct_switch = True
        self._disconnect = False
        self._tilt = 0
        self._basket_first_seek = True
        self._ball_first_seek = True
        self._sbrick_voltage = None
        self._m_left = None
        self._m_right = None
        self._m_camera = None
        self._s_accel = None
        self._s_color = None

    def _connect_to_sbrick_and_nxt(self):
        self.connect_to_sbrick()
        self.connect_to_nxt()

    def initiate_nxt_motors_and_sensors(self):
        self._m_left = Motor(self._nxt, PORT_B)
        self._m_right = Motor(self._nxt, PORT_C)
        self._m_camera = Motor(self._nxt, PORT_A)
        self._s_accel = MSACCL(self._nxt, PORT_1)
        self._s_color = HTColorv2(self._nxt, PORT_3)

    # stop all operations with all motors
    def stop(self, segmentation=False):
        if not segmentation:
            self._basket_first_seek = True
            self._ball_first_seek = True
        self._m_left.idle()
        self._m_right.idle()
        self._m_camera.idle()
        self._sbrick_client.publish_stop(self.SBRICK_MAC,
                                         channel_list=[self.DRIBLER_MOTOR, self.THROW_MOTOR])

    # driving according to joystick with point y,x
    def drive(self, x_vector, y_vector, in_operation):
        if x_vector != 0 or y_vector != 0:
            min_power = 60
            max_power = self.NXT_MAX_POWER
            pow_range = max_power - min_power
            pow_factor = abs(y_vector)
            direction = -1 if y_vector >= 0 else 1
            steering_factor = abs(x_vector) * (-direction)
            extra_power_r = pow_range * (pow_factor - (steering_factor if x_vector >= 0 else -steering_factor))
            extra_power_l = pow_range * (pow_factor - (steering_factor if x_vector < 0 else -steering_factor))
            right = int(direction * (min_power + extra_power_r))
            left = int(direction * (min_power + extra_power_l))
            r = min(right, max_power) if right > 0 else max(right, -max_power)
            l = min(left, max_power) if left > 0 else max(left, -max_power)
            if direction > 0:
                r, l = l, r
            self._m_right.run(r)
            self._m_left.run(l)
        elif not in_operation:
            self._m_right.brake()
            self._m_left.brake()

    def directed_drive(self, left_motor, right_motor, pull=False):
        if pull:
            self.pull()
            self._ball_first_seek = True
        self._m_left.run(int(left_motor) * (-1))
        self._m_right.run(int(right_motor) * (-1))

    def drive_back(self):
        self._m_left.run(self.NXT_MAX_POWER)
        self._m_right.run(self.NXT_MAX_POWER)

    # adjust camera according to specific angle with a specific motor
    # whenever replacing motor or tilt sensor needs to calibrate this method
    def adjust_camera(self, obj):
        tilt = self._s_accel.get_tilt('z')
        if obj == 'ball':
            degrees_to_rotate = 360 * ((tilt + 24) / 4 - 11) / 3
        elif obj == 'basket':
            degrees_to_rotate = 360 * ((tilt + 24) / 4 - 3) / 3
        else:
            return

        # direction of spin
        sign = -1
        if degrees_to_rotate < 0:
            degrees_to_rotate = -degrees_to_rotate
            sign = 1
        try:
            self._m_camera.turn(sign * self.NXT_MAX_POWER, int(degrees_to_rotate))
        except:
            print('cant turn camera...')

    def seek_for_ball(self):
        if self._ball_first_seek:
            self._ball_first_seek = False
            self.adjust_camera('ball')

        self._m_left.run(70)
        self._m_right.run(-70)

    def seek_for_basket(self):
        self._m_left.run(70)
        self._m_right.run(-70)

    def move_to_ball(self, x, y, radius):
        # x betwin 0 to WIDTH
        # x regulation because the camera is not in the center of the robot
        x = x - 20
        nx = abs(x - self.FRAME_WIDTH / 2)
        # the side regulation is because the linear function is too hard close to edges
        side_regulation = 10 if nx > self.FRAME_WIDTH / 4 else 0
        left_speed = self.NXT_MAX_POWER - (nx * 50) / (self.FRAME_WIDTH / 2) - side_regulation
        right_speed = self.NXT_MAX_POWER - (nx * 100) / (self.FRAME_WIDTH / 2)
        if x < self.FRAME_WIDTH / 2:
            temp = left_speed
            left_speed = right_speed
            right_speed = temp
        self.directed_drive(left_speed, right_speed, pull=True)

        # adjusting the camera according to the ball location on the screen
        if y > 300:
            self._m_camera.run(self.NXT_MAX_POWER)
        elif y < 100:
            self._m_camera.run(-self.NXT_MAX_POWER)
        elif y > 400 / 2 + 20:
            self._m_camera.run(70)
        elif y < 400 / 2 - 20:
            self._m_camera.run(-70)
        else:
            self._m_camera.brake()

    def move_to_basket(self, x, y, distance, angle):
        self._basket_first_seek = True
        correct_distance = config.SHOOTING_DISTANCE
        if distance < correct_distance - 10:
            self.drive_back()
        elif distance > correct_distance + 20:
            left_speed = self.NXT_MAX_POWER - (abs(x - self.FRAME_WIDTH / 2) * 50) / (self.FRAME_WIDTH / 2)
            right_speed = self.NXT_MAX_POWER - (abs(x - self.FRAME_WIDTH / 2) * 100) / (self.FRAME_WIDTH / 2)
            if x < self.FRAME_WIDTH / 2:
                temp = left_speed
                left_speed = right_speed
                right_speed = temp
            self.directed_drive(left_speed, right_speed)
        elif correct_distance - 2 < distance < correct_distance + 1:
            self._centerize_and_shoot(x, angle)
        else:
            if distance > correct_distance:
                self._m_right.run(-61)
                self._m_left.run(-61)
            else:
                self._m_right.run(61)
                self._m_left.run(61)

        # # for adjasting the camera while tracking, this is comment out because it makes the nxt work slowly
        # if y > 300:
        #     self.m_camera.run(self.NXT_MAX_POWER)
        # elif y < 100:
        #     self.m_camera.run(-self.NXT_MAX_POWER)
        # elif y > 400 / 2 + 20:
        #     self.m_camera.run(70)
        # elif y < 400 / 2 - 20:
        #     self.m_camera.run(-70)
        # else:
        #     self.m_camera.brake()

    def _centerize_and_shoot(self, x, angle):
        real_center = self.FRAME_WIDTH / 2
        self._m_left.brake()
        self._m_right.brake()
        regulation_from_angle = self._calc_regulation_from_angle(angle)
        diff = real_center - x - regulation_from_angle
        sign = 1 if diff < 0 else -1
        if abs(diff) > 2:
            # has found better to spin only with one motor
            self._m_left.run(-sign * 67)
        else:
            self.throw()

    # this regulation calculated from set of points:
    # 1. the angle between the top square of the basket and the floor.
    # 2. the center of the square.
    # adjusting the robot to be inline with the center of the basket
    def _calc_regulation_from_angle(self, angle):
        if angle < 20:
            return 43.3 / (1 + (angle / 2.06) ** 1.523) - 103.4
        if angle > 150:
            return 101.01 / (1 + (angle / 174.3) ** 54.16) - 75.04
        return 0

    def throw(self):
        full_voltage_throwing_power = 'f6'
        medium_voltage_throwing_power = 'f8'
        low_voltage_throwing_power = 'fa'
        throwing_power = full_voltage_throwing_power
        if self._sbrick_voltage:
            if self._sbrick_voltage > 8.1:
                pass
            elif self._sbrick_voltage > 8:
                throwing_power = medium_voltage_throwing_power
            elif self._sbrick_voltage > 7.5:
                throwing_power = low_voltage_throwing_power
            self._sbrick_voltage = None
        self._sbrick_client.publish_drive(self.SBRICK_MAC, self.THROW_MOTOR, self.THROW_OUT, throwing_power,
                                          self.THROW_WARMUP_TIME + 1)
        time.sleep(self.THROW_WARMUP_TIME)
        self._sbrick_client.publish_drive(self.SBRICK_MAC, self.PULLER_MOTOR, self.PULLER_OUT, self.MAX_POWER,
                                          self.SBRICK_NORMAL_WAITING_TIME)
        # waiting for the ball to be thrown out
        time.sleep(2)
        self._controller.notify_has_ball(False)

    def pull(self):
        dribler_pull_time = 2.5
        self._sbrick_client.publish_drive(self.SBRICK_MAC, self.PULLER_MOTOR, self.PULLER_IN, self.MAX_POWER,
                                          self.SBRICK_NORMAL_WAITING_TIME)
        self._sbrick_client.publish_drive(self.SBRICK_MAC, self.DRIBLER_MOTOR, self.DRIBLER_IN, self.MAX_POWER,
                                          dribler_pull_time)

    def is_contain_ball(self, color):
        color_pronounced_value = 60
        s_color = self._s_color.get_active_color()
        if color == 'red':
            return s_color.red > color_pronounced_value
        if color == 'blue':
            return s_color.blue > color_pronounced_value

    def turn(self, deg=90):
        # running back for a little
        self._m_left.run(self.NXT_MAX_POWER)
        self._m_right.run(self.NXT_MAX_POWER)
        time.sleep(0.3)

        regulation_for_turn = 1
        self._m_left.run(100)
        self._m_right.run(-100)
        time.sleep((regulation_for_turn + deg / 180) * .3)
        self._m_left.brake()
        self._m_right.brake()

    def update_thrower_voltage(self):
        respond = self._sbrick_client.rr_get_adc(self.SBRICK_MAC, timeout=5)
        parsed_json = json.loads(respond)
        good_response = 100
        if parsed_json['ret_code'] == good_response:
            self._sbrick_voltage = parsed_json['voltage']
            print(self._sbrick_voltage)
            return True
        print('cant get voltage')
        return False

    def connect(self):
        self._connect_to_sbrick_and_nxt()

    def set_controller(self, controller):
        self._controller = controller
