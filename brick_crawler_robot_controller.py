import time

import pygame

from brick_crawler_robot_model import BrickCrawlerRobotModel
from camera_model import CameraModel
import config
from controller import Controller


class BrickCrawlerRobotController(Controller):
    def __init__(self, brick_crawler_robot_model, camera_model):
        super().__init__()
        self._camera_model: CameraModel = camera_model
        self._camera_model.set_controller(self)
        self._robot_model: BrickCrawlerRobotModel = brick_crawler_robot_model
        self._robot_model.set_controller(self)
        self._stop = False
        self._horizontal_pos = 0
        self._current_prediction = None

        self.pool_pos = 3
        self.liftarm_pos = 1
        self.axel_pos = 3
        self.gear_pos = 2
        self.recognition_pos = 0

        self.LIFTARM_PREDICTION = 0
        self.AXEL_PREDICTION = 1
        self.GEAR_PREDICTION = 2

    def horizontal_move(self, pos):
        # resetting the position in case that its not already set
        if self._horizontal_pos is None:
            self._reset_horizontal_pos()
            print('reset horizontal pos')

        if pos not in range(4):
            print('Horizontal move value is invalid')
            return
        self._robot_model.horizontal_move(pos)
        self._horizontal_pos = pos

    def joystick_move(self, axis0, axis1):
        pass

    def stop_operation(self):
        print('Stopping operations')
        self._stop = True
        if self._camera_model:
            self._camera_model.stop_active_function()
        self._robot_model.stop()

    def initiate(self):
        self._robot_model.connect()

    def _get_horizontal_pos(self):
        return self._horizontal_pos

    def _reset_horizontal_pos(self):
        self._robot_model.reset_horizontal_pos()
        self._horizontal_pos = 0

    def automation_brick_sorting(self):
        self._stop = False

        self.autonomous_grab_and_leave_brick()
        time.sleep(2)
        self._camera_model.set_active_function(self._camera_model.brick_recognizer, self.notify_predication)
        time.sleep(2)
        pred = self._current_prediction
        print(pred)
        self._camera_model.stop_active_function()
        time.sleep(2)

        if pred == self.LIFTARM_PREDICTION:
            self.drop_liftarm()
        elif pred == self.AXEL_PREDICTION:
            self.drop_axel()
        elif pred == self.GEAR_PREDICTION:
            self.drop_gear()

    def notify_predication(self, predication):
        self._current_prediction = predication

    def autonomous_grab_and_leave_brick(self):
        self.horizontal_move(self.pool_pos)
        self.grab_brick()
        self.horizontal_move(self.recognition_pos)
        self.drop_brick()
        self.horizontal_move(self.pool_pos)

    def grab_brick(self):
        self._robot_model.grab_brick()

    def drop_brick(self):
        self._robot_model.drop_brick()

    def drop_liftarm(self):
        self.drop_to_pos(self.liftarm_pos)

    def drop_to_pos(self, pos):
        self.horizontal_move(self.recognition_pos)
        self.grab_brick_from_white_board()
        self.horizontal_move(pos)
        self.drop_brick()
        self.horizontal_move(self.pool_pos)

    def drop_axel(self):
        self.drop_to_pos(self.axel_pos)

    def drop_gear(self):
        self.drop_to_pos(self.gear_pos)

    def recognize_frame(self):
        self._camera_model.set_active_function(self._camera_model.brick_recognizer, self.notify_predication)

    def grab_brick_from_white_board(self):
        self._robot_model.grab_brick_from_white_board()

    def exit(self):
        self._camera_model.exit()
        self._robot_model.exit()

