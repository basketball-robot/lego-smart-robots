brick_crawler = False
black_mamba = False
holonomic = True

# VIDEO_SRC = 'http://10.42.0.233:8080/video' # one plus
VIDEO_SRC = 'http://192.168.1.15:8080/video'  # one plus wifi
if brick_crawler:
    VIDEO_SRC = 'http://192.168.1.20:8080/video'  # s6 wifi

NXT_MAC = '00:16:53:0A:9B:92'  # NXT
# NXT_MAC = '00:16:53:1A:30:E0' # get_map
if brick_crawler:
    NXT_MAC = '00:16:53:14:38:D6'  # third NXT

SBRICK_MAC = 'd0:cf:5e:53:7c:0f'  # 2
if brick_crawler or holonomic:
    SBRICK_MAC = '00:07:80:d0:40:75'  # 1

SHOOTING_DISTANCE = 62.5

FLIP_CAMERA = True
CAMERA_FOCAL = 568  # one plus
FRAME_WIDTH = 720
FRAME_HEIGHT = 480

BASKET_SQUARE_WIDTH = 17.4
BASKET_SQUARE_HEIGHT = 9
BASKET_SQUARE_HEIGHT_FROM_GROUND = 26
CAMERA_HEIGHT_FROM_GROUN = 15

BASKET_SQUARE_HSV_COLOR_LOWER = (100, 100, 40)  # one plus
BASKET_SQUARE_HSV_COLOR_UPPER = (130, 255, 255)  # one plus

LEGO_BALL_HSV_COLOR_LOWER = (0, 80, 0)  # one plus
LEGO_BALL_HSV_COLOR_UPPER = (12, 255, 255)  # one plus

PROJECT_PATH = '/home/kfir/PycharmProjects'

APPLY_ML_MODEL = True
ML_MODEL_NAME = 'floorModel4'
BRICK_MODEL_NAME = 'brick_model_4'

### joisticks

# red jostick
# A_BUTTON = 0
# B_BUTTON = 1
# X_BUTTON = 3
# Y_BUTTON = 4
# L1_BUTTON = 6
# L2_BUTTON = 8
# R1_BUTTON = 7
# R2_BUTTON = 9
# SELECT_BUTTON = 10
# START_BUTTON = 11

# wired joystick
A_BUTTON = 1
B_BUTTON = 2
X_BUTTON = 0
Y_BUTTON = 3
L1_BUTTON = 4
L2_BUTTON = 6
R1_BUTTON = 5
R2_BUTTON = 7
BACK_BUTTON = 8
START_BUTTON = 9
