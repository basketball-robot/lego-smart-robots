import time

from holonomic_robot_model import HolonomicRobotModel
from controller import Controller


class HolonomicRobotController(Controller):
    def __init__(self, holonomic_robot_model):
        super().__init__()
        # self._camera_model: CameraModel = camera_model
        # self._camera_model.set_controller(self)
        self._robot_model: HolonomicRobotModel = holonomic_robot_model
        self._robot_model.set_controller(self)
        self._stop = False

    def joystick_move(self, axis0, axis1, l1, r1):
        self._robot_model.holonomic_drive(axis0, axis1, l1, r1)

    def stop_operation(self):
        print('Stopping operations')
        self._stop = True
        # if self._camera_model:
        #     self._camera_model.stop_active_function()
        self._robot_model.stop()

    def initiate(self):
        self._robot_model.connect()

    def exit(self):
        # self._camera_model.exit()
        self._robot_model.exit()

