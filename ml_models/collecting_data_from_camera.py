import argparse
import time
import cv2
from imutils.video import WebcamVideoStream
import os
import sys
sys.path.append('../')
# # sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '.')))
# from os import path
# sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
# import sys
sys.path.append(os.getcwd())

def get_arguments():
    ap = argparse.ArgumentParser()
    ap.add_argument('-w', '--webcam', required=False,
                    help='Use webcam')
    args = vars(ap.parse_args())

    return args


def main():
    args = get_arguments()
    video_src = args['webcam']
    video_stream = WebcamVideoStream(video_src).start()
    # allow the camera to warm up
    time.sleep(2.0)

    i = 1
    while i < 3000:
        frame = video_stream.read()
        if frame is None:
            break
        frame = cv2.flip(frame, 0)
        frame = cv2.resize(frame, (128, 128), interpolation=cv2.INTER_AREA)
        print(cv2.imwrite('data/' + str(i) + '.png', frame))
        time.sleep(0.2)
        i += 1


if __name__ == '__main__':
    main()
