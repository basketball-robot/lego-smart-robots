import abc
import threading
import time
import nxt.bluesock

import nxt

import config
from lib.sbrick_m2mipc import SbrickIpcClient
from robot_model import RobotModel


class BasketballRobotModel(RobotModel):
    def __init__(self):
        super().__init__()
        self.SBRICK_NORMAL_WAITING_TIME = 0.5
        self.MAX_POWER = None
        self.PULLER_IN = None
        self.DEMON_MOTOR = None
        self._disconnect = None
        self._sbrick_client = None
        self._nxt = None
        self.C_WISE = '00'
        self.CC_WISE = '01'
        self.MAX_POWER = 'ff'
        self.MID_POWER = 'Ff'
        self.REVERSE_POWER = 'FB'
        self.LOW_POWER = 'B0'


    @abc.abstractmethod
    def connect(self):
        pass

    # stop all operations with all motors
    @abc.abstractmethod
    def stop(self):
        pass

    # driving according to joystick with point y,x
    @abc.abstractmethod
    def drive(self, *args):
        pass

    @abc.abstractmethod
    def directed_drive(self, *args):
        pass

    # adjust camera according to specific angle with a specific motor
    # whenever replacing motor or tilt sensor needs to calibrate this method
    @abc.abstractmethod
    def adjust_camera(self, *args):
        pass

    @abc.abstractmethod
    def seek_for_ball(self):
        pass

    @abc.abstractmethod
    def seek_for_basket(self):
        pass

    @abc.abstractmethod
    def move_to_ball(self, *args):
        pass

    @abc.abstractmethod
    def move_to_basket(self, *args):
        pass

    @abc.abstractmethod
    def throw(self):
        pass

    @abc.abstractmethod
    def pull(self):
        pass

    @abc.abstractmethod
    def is_contain_ball(self, *args):
        pass

    @abc.abstractmethod
    def turn(self, *args):
        pass

    @abc.abstractmethod
    def update_thrower_voltage(self):
        pass

    @abc.abstractmethod
    def set_controller(self, controller):
        pass

    @abc.abstractmethod
    def initiate_nxt_motors_and_sensors(self):
        pass

    def connect_to_sbrick(self):
        threading.Thread(target=self._start_sbrick_server).start()
        self._sbrick_client = SbrickIpcClient(broker_ip='127.0.0.1', broker_port=1883)
        self._sbrick_client.connect()
        time.sleep(5)
        threading.Thread(target=self._sbrick_demon).start()

    def connect_to_nxt(self):
        success = False
        while not success:
            try:
                self._nxt = nxt.bluesock.BlueSock(config.NXT_MAC).connect()
                success = True
            except:
                print('cant connect to nxt, reconnecting')

        self.initiate_nxt_motors_and_sensors()

    def _disconnect_from_sbrick(self):
        if self._sbrick_client is not None:
            self._sbrick_client.disconnect()

    # this demon is because the sbrick disconnect after a second without order
    # ordering the demon motor that is not connected and have no influence
    def _sbrick_demon(self):
        while not self._disconnect:
            self._sbrick_client.publish_drive(self.SBRICK_MAC, self.DEMON_MOTOR, self.PULLER_IN, self.MAX_POWER, 0.1)
            time.sleep(self.SBRICK_NORMAL_WAITING_TIME)
