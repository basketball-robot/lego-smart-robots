import abc
from abc import ABC

import pygame


class Controller(ABC):
    def __init__(self):
        self._screen = None

    @abc.abstractmethod
    def joystick_move(self, *args):
        pass

    @abc.abstractmethod
    def stop_operation(self):
        pass
    
    @abc.abstractmethod
    def initiate(self):
        pass

    @abc.abstractmethod
    def exit(self):
        pass

    def show_frame(self, frame):
        if self._screen:
            frame = pygame.surfarray.make_surface(frame)
            self._screen.blit(frame, (0, 0))
        else:
            print('No screen was set')

    def set_screen(self, screen):
        self._screen = screen

