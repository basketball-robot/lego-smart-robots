import math
import threading

import config

from utils.geometry_utils import is_normal_shape, intersection, degree_with_floor, rectangle_get_top_left_down_right
from utils.ml_utils import is_floor
from imutils.video import WebcamVideoStream
import numpy as np
import cv2
import imutils
import time
from utils import distance_calculator as dc
from utils.image_processing_utils import crop_non_white_object, grab_contours_from_color_hsv
from utils.ml_utils import predict_brick


class CameraModel:
    def __init__(self):
        self._src = config.VIDEO_SRC
        self._stop = False
        self._controller = None
        self._tracking_ball = False
        self._tracking_basket = False
        self._tracking_segmentation_ball = False
        self._video_stream = None
        self._set_video_source(self._src)
        self.video_loop()
        self._active_function = None
        self._callback_function = None

    def set_controller(self, controller):
        self._controller = controller

    def restart_loop(self):
        self._stop = True
        time.sleep(0.05)
        self._stop = False
        self.video_loop()
        time.sleep(0.05)

    def _video_loop(self):
        # keep looping until stop
        while not self._stop:
            frame = self._video_stream.read()

            if frame is None:
                break
            #  if the camera is fliped
            if config.FLIP_CAMERA:
                frame = cv2.flip(frame, -1)
            if self._tracking_ball:
                self.find_ball(frame)
            if self._tracking_basket:
                self.find_basket(frame)
            if self._tracking_segmentation_ball:
                self.find_segmentation_ball(frame)

            if self._active_function:
                self._callback_function(self._active_function(frame))

            cv2.waitKey(1)
            self.show_frame(frame)

        # clearing the video stream that it could restart if needed.
        # else it means that there are error with capturing the video source
        # and need a reset.
        if self._video_stream is not None:
            self._video_stream.stop()
        else:
            self._set_video_source(self._src)
            self._video_loop()
        cv2.destroyAllWindows()

    def video_loop(self):
        threading.Thread(target=self._video_loop).start()

    def set_tracking(self, object, val):
        if object == 'ball' or object == 'all':
            self._tracking_ball = val
        if object == 'basket' or object == 'all':
            self._tracking_basket = val
        if object == 'segmentation_ball' or object == 'all':
            self._tracking_segmentation_ball = val

    def find_ball(self, frame):
        blurred = cv2.GaussianBlur(frame, (11, 11), 0)
        # convert to HSV color space
        hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
        # construct a mask for the color, then perform
        mask = cv2.inRange(hsv, config.LEGO_BALL_HSV_COLOR_LOWER, config.LEGO_BALL_HSV_COLOR_UPPER)
        # dilations and erosions for cleaning the mask
        mask = cv2.erode(mask, None, iterations=2)
        mask = cv2.dilate(mask, None, iterations=2)
        # find contours in the mask and initialize the current
        # (x, y) center of the ball, and the ball list.
        cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
        center = None
        ball_list = []
        ball_number = 1
        sorted(cnts, key=cv2.contourArea)
        for contour in cnts:
            # approximate vertexes from the contour.
            approx = cv2.approxPolyDP(contour, 0.01 * cv2.arcLength(contour, True), True)
            area = cv2.contourArea(contour)
            (x, y), radius = cv2.minEnclosingCircle(contour)
            center = (int(x), int(y))
            circle_area = radius * radius * np.pi
            # ensuring that the contour is ball shape by large number of vertexes
            # and area of circle and the radius is not too small.
            if (len(approx) > 8) and (circle_area * 0.65 < area < circle_area * 1.2) and radius > 7:
                ball_list.append((center, int(radius), ball_number))
                ball_number += 1
        for ball in ball_list:
            cv2.putText(frame, "%.f" % ball[2], ball[0], cv2.FONT_HERSHEY_SIMPLEX, 2.0, (0, 255, 255), 3)
            cv2.circle(frame, ball[0], ball[1], (0, 255, 255), 2)
            cv2.circle(frame, ball[0], 5, (0, 0, 255), -1)
        # notify controller with the found balls
        self._notify_ball(ball_list)
        if len(ball_list) > 0:
            return True
        return False

    def find_segmentation_ball(self, frame):
        if self.find_ball(frame):
            self.set_tracking('all', False)
            self.set_tracking('ball', True)
        else:
            self.set_tracking('ball', False)

        # ask the ML floor model if its only floor and do not need to recognize obstacle
        if config.APPLY_ML_MODEL and is_floor(frame):
            self._controller.notify_obstacle(False)
            return

        step_size = 8
        obstacles_array = []

        # https://www.programmersought.com/article/7671791785/
        blur_gray = cv2.medianBlur(frame, 35)
        gray = cv2.cvtColor(blur_gray, cv2.COLOR_BGR2GRAY)
        ret, thresh = cv2.threshold(
            gray, 230, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
        # eliminate noise
        kernel = np.ones((3, 3), np.uint8)
        opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=3)
        sure_bg = cv2.dilate(opening, kernel, iterations=3)
        imgEdge = imutils.auto_canny(sure_bg)

        imagewidth = imgEdge.shape[1] - 1
        imageheight = imgEdge.shape[0] - 1
        obstacle = False

        # for the width of image array
        for j in range(0, imagewidth, step_size):
            # step through every pixel in height of array from bottom to top
            for i in range(imageheight - 5, 0, -1):
                # ignore first couple of pixels as may trigger due to undistorted
                # check to see if the pixel is white which indicates an edge has been found
                if imgEdge.item(i, j) == 255:
                    # notify obstacle only when it is close to the camera
                    if i > imageheight * 0.65 and imagewidth * 2 / 3 > j > imagewidth / 3:
                        obstacle = True
                    # if it is, add x,y coordinates to obstacle array
                    obstacles_array.append((j, i))
                    # if white pixel is found, skip rest of pixels in column
                    break
            else:  # no white pixel found
                # if nothing found, assume no obstacle. Set pixel position way off the screen to indicate
                # no obstacle detected.
                obstacles_array.append((j, 0))
        # draw lines between points in obstacle array
        for x in range(len(obstacles_array) - 1):
            cv2.line(frame, obstacles_array[x], obstacles_array[x + 1], (0, 255, 0), 1)
        # draw lines from bottom of the screen to points in obstacle array
        for x in range(len(obstacles_array)):
            cv2.line(frame, (x * step_size, imageheight), obstacles_array[x], (0, 255, 0), 1)

        self._controller.notify_obstacle(obstacle)

    def find_basket(self, frame):
        blurred = cv2.GaussianBlur(frame, (11, 11), 0)
        # convert to HSV color space
        hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
        # construct a mask for the color, then perform
        # a series of dilations and erosions to remove any small blobs left in the mask
        mask = cv2.inRange(hsv, config.BASKET_SQUARE_HSV_COLOR_LOWER, config.BASKET_SQUARE_HSV_COLOR_UPPER)
        mask = cv2.erode(mask, None, iterations=2)
        mask = cv2.dilate(mask, None, iterations=2)
        cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        cnts = imutils.grab_contours(cnts)

        # sorting with big area first
        cnts = sorted(cnts, key=cv2.contourArea, reverse=True)[:5]
        is_there_basket = False
        arr_of_shapes = []
        for c in cnts:
            # for each contour approximate vertexes according to the perimeter.
            perimeter = cv2.arcLength(c, True)
            arr_of_shapes.append(cv2.approxPolyDP(c, 0.06 * perimeter, True))
        height_limit = config.FRAME_HEIGHT * 0.65
        min_area = (config.FRAME_HEIGHT * config.FRAME_WIDTH) * 0.001591435
        # takes only the ones with 4 vertexes and higher from limit of the frame and the area is more then minimum.
        approx_squares = [i for i in arr_of_shapes if
                          len(i) == 4 and i[0][0][1] < height_limit and dc.area_of_rectangle(i) > min_area]

        def height_sorter(x):
            return x[0][0][1]

        def ratio_sorter(x):
            w, h = dc.dimension_of_rectangle(x)
            basket_square_ratio = config.BASKET_SQUARE_WIDTH / config.BASKET_SQUARE_HEIGHT
            return (w / h - basket_square_ratio) ** 2

        # top frame is zero so no need to reverse!
        approx_squares = sorted(approx_squares, key=height_sorter)[:2]
        # need to minimize the deviation in ratio of the basket square, so no need to reverse!
        approx_squares = sorted(approx_squares, key=ratio_sorter)[:1]

        for square in approx_squares:
            # checking for a normal square (not one vertex inside the other three vertexes)
            if is_normal_shape(square[0][0], square[1][0], square[2][0], square[3][0]):
                x_max = max([square[i][0][0] for i in range(4)])
                x_min = min([square[i][0][0] for i in range(4)])
                predict_width = int(abs(x_max - x_min))
                min_width_limit = config.FRAME_WIDTH * 0.041666667
                # if there is a square with width bigger then minimum limit assume that this is the basket
                if predict_width > min_width_limit:
                    # calculating degree with the floor to enclose degree with basket with scale of 2.5
                    angle = degree_with_floor(square[0][0], square[1][0], square[2][0], square[3][0])
                    angle = 180 - angle
                    angle_for_scale = angle
                    if angle_for_scale > 90:
                        angle_for_scale = 180 - angle_for_scale
                    if angle_for_scale > 3:
                        predict_width = predict_width / (math.cos(np.deg2rad(angle_for_scale * 6)))
                    is_there_basket = True
                    cv2.drawContours(frame, [square], -1, (0, 255, 0), 3)

                    predict_real_distance = dc.distance_to_camera(config.BASKET_SQUARE_WIDTH, config.CAMERA_FOCAL,
                                                                  predict_width)
                    height = config.BASKET_SQUARE_HEIGHT_FROM_GROUND - config.CAMERA_HEIGHT_FROM_GROUN
                    inner = predict_real_distance ** 2 - height ** 2
                    if inner <= 0:
                        horizontal_distance = -1
                    else:
                        # calculating horizontal distance according to Pitagoras
                        horizontal_distance = math.sqrt(predict_real_distance ** 2 - height ** 2)
                    # draw the horizontal distance on the frame.
                    cv2.putText(frame, "%.2f_cm" % horizontal_distance, (0, 60), cv2.FONT_HERSHEY_SIMPLEX, 2.0,
                                (0, 255, 255), 3)
                    center = cx, cy = intersection(square[0][0], square[1][0], square[2][0], square[3][0])

                    # # approximating center according the max enclosing rectangle
                    # center = cx, cy = int((square[0][0][0] + square[2][0][0]) / 2), int(
                    #     (square[1][0][1] + square[3][0][1]) / 2)
                    cv2.circle(frame, center, 5, (0, 255, 0), -1)

                    cv2.putText(frame, "%.2f_deg" % angle,
                                (0, 160), cv2.FONT_HERSHEY_SIMPLEX,
                                2.0, (130, 50, 50), 3)

        if is_there_basket:
            self._controller.notify_basket(int(cx), int(cy), horizontal_distance, angle)
        else:
            self._controller.notify_basket()  # no basket

    def _set_video_source(self, src=None):
        self._video_stream = WebcamVideoStream(src).start()
        # allow the camera to warm up
        time.sleep(2.0)

    def stop(self):
        self._stop = True

    def show_frame(self, frame):
        # adjusting frame to pygame format
        frame = np.rot90(frame)
        frame = cv2.flip(frame, 0)
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        self._controller.show_frame(frame)

    def _notify_ball(self, ball_list):
        self._controller.notify_ball(ball_list)

    def set_active_function(self, function, callback_function):
        self._active_function = function
        self._callback_function = callback_function

    def stop_active_function(self):
        self._active_function = None

    def brick_recognizer(self, frame):
        # finding white color enclosing rect for detecting the part inside of it
        color_lower = (0, 0, 150)
        color_upper = (255, 92, 255)

        cnts = grab_contours_from_color_hsv(frame, color_lower, color_upper)

        cnts = sorted(cnts, key=cv2.contourArea, reverse=True)[:2]
        arr_of_shapes = []
        for ap in cnts:
            peri = cv2.arcLength(ap, True)
            arr_of_shapes.append(cv2.approxPolyDP(ap, 0.06 * peri, True))
        approx = [i for i in arr_of_shapes if len(i) == 4]

        approx = sorted(approx, key=cv2.contourArea)[:2]
        for ap in approx:
            if len(ap) != 4:
                continue

            x_max = max([ap[i][0][0] for i in range(4)])
            x_min = min([ap[i][0][0] for i in range(4)])
            approx_width = int(abs(x_max - x_min))

            if approx_width < 100:
                continue

            # cropping according to enclosing white rect
            top_left, bottom_right = rectangle_get_top_left_down_right([ap[i][0] for i in range(4)])
            cropped = frame[top_left[1]:bottom_right[1], top_left[0]:bottom_right[0]]
            resized = cv2.resize(cropped, (296, 296), interpolation=cv2.INTER_AREA)
            # cropping the frames
            cropped_again = resized[20:276, 20:276]

            # masking the white to be real white
            im_color = cropped_again
            im_gray = cv2.cvtColor(im_color, cv2.COLOR_BGR2GRAY)
            _, mask = cv2.threshold(im_gray, thresh=180, maxval=255, type=cv2.THRESH_BINARY)
            mask3 = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)
            im_thresh_color = cv2.bitwise_or(im_color, mask3)

            cropped_super = crop_non_white_object(im_thresh_color)

            pred = predict_brick(cropped_super)
            # writing the label on the screen
            label = "liftarm" if pred == 0 else "axel" if pred == 1 else "gear"
            color = (0, 0, 255) if pred == 0 else (0, 255, 0) if pred == 1 else (255, 0, 0)
            cv2.putText(cropped_super, label, (3, 40), cv2.FONT_HERSHEY_SIMPLEX, 2,
                        color, 2)
            cv2.imshow("cropped", cropped_super)

            return pred

    def exit(self):
        self._stop = True
